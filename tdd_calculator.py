import re

class Calculator:
    @staticmethod
    def Calculate(input):
        if len(input) == 0:
            return 0

        result = 0
        input = Calculator.get_separator(input)
        for x in input.split(','):
            tmp = int(x)
            if tmp >= 1000:
                continue
            if tmp < 0:
                raise Exception('negative number are not allowed')
            result += int(x)

        return result

    @staticmethod
    def get_separator(input):
        if input.startswith('//'):
            separators, numbers = input[2:].split('\n')
            if len(separators) == 1:
                return numbers.replace(separators, ',')

            for s in separators.split(','):
                if s.startswith('[') and s.endswith(']'):
                    numbers = numbers.replace(s[1:-1], ',')
            return numbers 
        elif '\n' in input:
            return input.replace('\n', ',')
        return input

Calculator.Calculate('//#\n5#13#12')
