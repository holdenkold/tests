import unittest
import sys
sys.path.insert(0, '.')
from tdd_calculator import Calculator

class TestCalculate(unittest.TestCase):
    # 1. An empty string returns zero
    def test_empty_string(self):
        #given
        empty_string = ''
        expected = 0
        #when
        result = Calculator.Calculate(empty_string)
        #then
        self.assertEqual(expected, result)

    # 2. A single number returns the value
    def test_single_number(self):
        #given
        single_number = '5'
        expected = 5
        #when
        result = Calculator.Calculate(single_number)
        #then
        self.assertEqual(expected, result)
    
    # 3. Three numbers, comma delimited, returns the sum
    def test_multiple_numbers(self):
        #given
        multiple_numbers = '3,45,7'
        expected = 55
        #when
        result = Calculator.Calculate(multiple_numbers)
        #then
        self.assertEqual(expected, result)

    # 4. Three numbers, newline delimited, returns the sum
    def test_line_separator(self):
        #given
        line_separator = '10\n5\n7'
        expected = 22
        #when
        result = Calculator.Calculate(line_separator)
        #then
        self.assertEqual(expected, result)
    
    # 5. Three numbers, delimited either way, returns the sum
    def test_mixed_separator(self):
        #given
        mixed_separator = '10\n5,7'
        expected = 22
        #when
        result = Calculator.Calculate(mixed_separator)
        #then
        self.assertEqual(expected, result)

    # 6. Negative numbers throw an exception
    def test_negative_numbers(self):
        #given
        negative_numbers = '3,-45,7'
        #then
        self.assertRaises(Exception, Calculator.Calculate, negative_numbers)
    
    # 7. Numbers greater than 1000 are ignored
    def test_skip_greater_then_1000(self):
        #given
        multiple_numbers = '4,1233,10'
        expected = 14
        #when
        result = Calculator.Calculate(multiple_numbers)
        #then
        self.assertEqual(expected, result)

    # 8. A single char delimiter can be defined on the first line
    def test_single_separator(self):
        #given
        single_separator = '//#\n5#13#12'
        expected = 30
        #when
        result = Calculator.Calculate(single_separator)
        #then
        self.assertEqual(expected, result)

    # 9. A multi char delimiter can be defined on the first line
    def test_diff_separator(self):
        #given
        diff_separator = '//[###]\n4###13###10'
        expected = 27
        #when
        result = Calculator.Calculate(diff_separator)
        #then
        self.assertEqual(expected, result)
    
    # 10. Many single or multi-char delimiters can be defined
    def test_array_separator(self):
        #given
        array_separator = '//[###],[,],[???]\n4,13???10'
        expected = 27
        #when
        result = Calculator.Calculate(array_separator)
        #then
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()                                                      